package console;

import java.io.File;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import database.QueryUtil;
import database.SQLCMD;
import preprocess.StanfordNLP;
import LDA.TopicModel;
import edu.stanford.nlp.ling.TaggedWord;
import fpmin.FpGrowth;
import fpmin.FpNode;
import fpmin.FrequentPattern;
import utility.CSVParser;
import utility.DocumentMetricComparator;
import utility.RegularExpression;
import utility.TextUtil;
import model.BigramRecord;
import model.Document;
import model.PostRecord;
import model.TopicSet;

public class CommonDefectAnalyzerNew {
	public static final double DEFECT_CRITERION = 0;
	private static SQLCMD db = new SQLCMD("toyota", "128.173.49.48", 3306,
			"dapj", "cs5525");

	public static final String MANUFACTUER = "Toyota";
	public static final String MODEL = "Camry";

	public static final String[] STOPWORDS = { "car", "camry", "problem",
			"issue", "anything", "time", "help", "something", "Hi,", "way",
			"anyone", "someone", "question", "year", "job", "everything",
			"nothing", "Hi", "everyone", "dealer", "home", "owner", "friend",
			"??", "L", "forum", "today", "idea", "week", "advice",
			"experience", "month", "DIY", "toyota", "day", "SE", "anybody",
			"%", "lot", "bit", "http", "thing", "se", "price", "brand", "step",
			"dealership", "wife", "v6", "kind", "i4", "couple", "noise",
			"change", "thread", "option", "Im", "gen", "work", "fine", "while",
			"side", "leak", "accord", "deal", "post", "cost", "end" };

	public static final String[] INCOMPLETE_PROPERTY = { "part", "oil" };

	public static Map<String, Double> getComponentNumMap(
			Map<String, List<PostRecord>> threadsOfModels) {
		Map<String, Double> result = new HashMap<String, Double>();

		File threadNoun = new File("result/ThreadNoun.csv");

		for (String modelYear : threadsOfModels.keySet()) {
			try {
				StanfordNLP stNLP = StanfordNLP.getInstance();

				// Extract posts
				List<PostRecord> postList = threadsOfModels.get(modelYear);

				// Find posts discussing defects
				List<Document> docList = findDefectPosts(postList,
						GlobalConstants.SMOKE_BIGRAM_URL);
				Collections.sort(docList,
						new DocumentMetricComparator<Document>());

				PrintWriter pw2 = new PrintWriter(threadNoun);

				int num = 1;

				DecimalFormat df = new DecimalFormat("0.0000");
				// Extract flawed components mentioned by the posts
				for (Document doc : docList) {
					String strNum = String.format("%04d", num);
					System.out.println(strNum + "\t\t"
							+ df.format(doc.getMetrics()) + "\t\t"
							+ doc.getTitle() + "\t\t" + doc.getContent());

					// Do POS-tagging to words
					List<ArrayList<TaggedWord>> taggedSenList = stNLP
							.POSTag(doc.getContent());

					mergeAdjacentPOS(taggedSenList);

					// Take sentences as transactions for item set mining
					for (ArrayList<TaggedWord> sentence : taggedSenList) {
						StringBuffer buffer = new StringBuffer();
						for (TaggedWord tw : sentence) {
							String tag = tw.tag();
							String value = tw.value().toLowerCase();
							// Only keep regular nouns
							if (tag.equalsIgnoreCase("NN")
									&& !CommonDefectAnalyzerNew
											.hasStopWord(value))
								if (sentence.indexOf(tw) < sentence.size() - 1)
									buffer.append(value + "::");
								else
									buffer.append(value);
						}

						String nounStr = buffer.toString().trim();
						if (nounStr.length() > 0)// Skip empty lines
							pw2.println(nounStr);
					}

					num++;
				}
				pw2.close();

				// Find the frequent patterns by the FP-Growth algorithm
				Map<String, Long> featureMap = findFrequenPatterns(threadNoun);
				Map<String, Long> cleanedFeatureMap = new HashMap<String, Long>();

				// Filter the incomplete properties
				for (String item : featureMap.keySet()) {
					if (!isIncompleteProperty(item))
						cleanedFeatureMap.put(item, featureMap.get(item));
				}
				printFrequentPatterns(cleanedFeatureMap);
				result.put(modelYear, new Double(cleanedFeatureMap.keySet().size()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		return result;
	}

	/**
	 * Find the posts revealing defect by "Smoky words"
	 * 
	 * @param postCSV
	 * @param smokeWordCSV
	 * @return
	 */
	public static List<Document> findDefectPosts(List<PostRecord> posts,
			String smokeWordCSV) {
		List<Document> result = new ArrayList<Document>();
		String[] smokeWordColumns = { "bigram", "rsv" };
		List<Object> bigrams = CSVParser.retrieveDataFromCSV(smokeWordCSV,
				BigramRecord.class, smokeWordColumns);

		for (Object post : posts) {
			PostRecord pr = (PostRecord) post;
			// System.out.println(pr.getTitle() + "\t\t" + pr.getTime() + "\t\t"
			// + pr.getContent());
			double defectLevel = 0;
			String title = pr.getTitle();
			String content = pr.getContent();
			String id = pr.getId();
			String strTime = pr.getTime();

			String text = title + " " + content;
			text = text.toLowerCase();

			// Search all the bigrams in the title and 1st post of the thread
			// Calculate the sum of metrics
			for (Object bigram : bigrams) {
				BigramRecord br = (BigramRecord) bigram;
				String bgText = br.getBigram();
				double rsv = br.getRsv();
				if (text.indexOf(bgText) >= 0) {
					defectLevel += rsv;
				}
			}

			long postID = 0;
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"MM-dd-yyyy, hh:mm aa");
			Date time = null;
			try {
				postID = Long.parseLong(id);
				time = dateFormat.parse(strTime);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}

			if (defectLevel > DEFECT_CRITERION) {
				Document doc = new Document(title, content, defectLevel, time,
						postID);
				result.add(doc);
			}

		}

		System.out.println(result.size() + "/" + posts.size()
				+ " flawed posts extracted");

		return result;
	}

	/**
	 * Merge the adjacent words with same POS, for example, merge the adjacent
	 * "oil" and "filter" into "oil filter"
	 * 
	 * @param taggedSenList
	 */
	private static void mergeAdjacentPOS(
			List<ArrayList<TaggedWord>> taggedSenList) {
		for (int i = 0; i < taggedSenList.size(); i++) {

			String comWord = taggedSenList.get(i).get(0).word();// word after
																// combination
			String tag = taggedSenList.get(i).get(0).tag(); // indicate type of
															// previous word

			// for each word in this sentence
			for (int j = 1; j < taggedSenList.get(i).size(); j++) {
				String word = taggedSenList.get(i).get(j).word();
				String category = taggedSenList.get(i).get(j).tag();
				// System.out.println(tag + " : " + category + " : " +
				// sentence);

				// combine continuous same-type words
				if (category.equals(tag)) {
					comWord = comWord + " " + word;
					taggedSenList.get(i).remove(j);
					j = j - 1;
					taggedSenList.get(i).get(j).setWord(comWord);
				} else
					comWord = word;

				tag = category;
			}
		}
	}

	/**
	 * Call the FPGrowth algorithm and find the frequent item sets in the
	 * specified file
	 * 
	 * @param file
	 */
	public static Map<String, Long> findFrequenPatterns(File file) {
		Map<String, Long> result = new HashMap<String, Long>();
		FrequentPattern[] patterns = FpGrowth.getFrequentPatterns(file
				.getAbsolutePath());
		if (patterns == null)
			System.err.println("NULL patterns!");
		// for(FrequentPattern pattern : patterns){
		// System.out.println(pattern.getNodeSet() + "\t\t" +
		// pattern.getFrequency());
		// }

		for (FrequentPattern rule : patterns) {
			// Keep the nodes which have multiple words or high frequency
			if (rule.getNodeSet().size() == 1
					&& (FpGrowth.maxNodeLength(rule) > 1 || rule.getFrequency() >= FpGrowth.absSupport * 7)) {
				FpNode node = rule.getNodeSet().iterator().next();
				String idName = node.getIdName();
				long frequency = rule.getFrequency();
				result.put(idName, new Long(frequency));
				// System.out.println(idName + "\t:\t" + rule.getFrequency());
			}
		}
		return result;
	}

	public static void printFrequentPatterns(Map<String, Long> map) {
		for (String pattern : map.keySet()) {
			System.out.println(pattern + "\t:\t" + map.get(pattern));
		}
	}

	/**
	 * Check whether the specified word is stop-word
	 * 
	 * @param value
	 * @return
	 */
	public static boolean hasStopWord(String value) {
		if (value == null || value.length() == 0)
			return true;
		String[] array = value.split(" ");
		for (String item : array) {
			for (String word : STOPWORDS) {
				if (word.equalsIgnoreCase(item))
					return true;
			}
		}
		return false;
	}

	private static List<String> componentsInDocument(Document doc,
			Set<String> featureSet) {
		List<String> result = new ArrayList<String>();
		List<String> candidates = new ArrayList<String>();

		String title = doc.getTitle();
		String content = doc.getContent();

		// Search components appearing in the document
		for (String feature : featureSet) {
			if (title.indexOf(feature) >= 0)
				candidates.add(feature);
			if (content.indexOf(feature) >= 0)
				candidates.add(feature);
		}

		// Prune duplicate components
		for (int i = 0; i < candidates.size(); i++) {
			String candidateItem = candidates.get(i);
			boolean parentExist = false;
			for (int j = 0; j < candidates.size(); j++) {
				String curItem = candidates.get(j);
				if (j != i && contains(curItem, candidateItem))
					parentExist = true;
			}
			if (!parentExist)
				result.add(candidateItem);
		}

		return result;
	}

	public static boolean contains(String parent, String child) {
		if (parent.length() > child.length() && parent.indexOf(child) >= 0)
			return true;
		return false;
	}

	/**
	 * Extract post records from CSV file according to the specified path
	 * 
	 * @param path
	 * @return
	 */
	public static List<PostRecord> extractPostsFromCSV(String path) {
		String[] postColumns = { "title", "time", "content", "id" };
		List<Object> posts = CSVParser.retrieveDataFromCSV(
				GlobalConstants.POST_URL, PostRecord.class, postColumns);
		List<PostRecord> postList = new ArrayList<PostRecord>();
		for (Object obj : posts) {
			postList.add((PostRecord) obj);
		}

		return postList;
	}

	/**
	 * Check whether one item is in the incomplete property list
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isIncompleteProperty(String str) {
		for (String item : INCOMPLETE_PROPERTY) {
			if (item.equalsIgnoreCase(str))
				return true;
		}

		return false;
	}

	public static List<Document> getDocumentOfYear(List<Document> docList,
			int year) {
		List<Document> result = new ArrayList<Document>();
		for (Document doc : docList) {
			int docYear = TextUtil.extractYear(doc);
			if (year == docYear)
				result.add(doc);
		}

		System.out.println("\n" + result.size() + "/" + docList.size()
				+ " posts identified for YEAR " + year + "\n");
		return result;
	}

	public static void main(String[] args) {
		int forumID = 18;
		int yearProduct = 2012;

		File threadFile = new File("result/ThreadContent_3_4.csv");
		File threadNoun = new File("result/ThreadNoun_3_4.csv");

		try {
			StanfordNLP stNLP = StanfordNLP.getInstance();

			// Extract posts
			List<PostRecord> postList = QueryUtil.extractPostsFromDB(db,
					forumID);

			// Find posts discussing defects
			List<Document> docList = findDefectPosts(postList,
					GlobalConstants.SMOKE_BIGRAM_URL);
			Collections.sort(docList, new DocumentMetricComparator<Document>());

			PrintWriter pw1 = new PrintWriter(threadFile);
			PrintWriter pw2 = new PrintWriter(threadNoun);

			// TODO: For test only. Filter everything except the posts talking
			// about vehicles produced in the specified year
			docList = getDocumentOfYear(docList, yearProduct);

			int num = 1;

			DecimalFormat df = new DecimalFormat("0.0000");
			// Extract flawed components mentioned by the posts
			for (Document doc : docList) {
				String strNum = String.format("%04d", num);
				System.out.println(strNum + "\t\t"
						+ df.format(doc.getMetrics()) + "\t\t" + doc.getTitle()
						+ "\t\t" + doc.getContent());

				pw1.println("\"" + strNum + "\"" + "," + "\"" + doc.getTitle()
						+ "\"" + "," + "\"" + doc.getContent() + "\"");

				// Do POS-tagging to words
				List<ArrayList<TaggedWord>> taggedSenList = stNLP.POSTag(doc
						.getContent());

				mergeAdjacentPOS(taggedSenList);

				// Take sentences as transactions for item set mining
				for (ArrayList<TaggedWord> sentence : taggedSenList) {
					StringBuffer buffer = new StringBuffer();
					for (TaggedWord tw : sentence) {
						String tag = tw.tag();
						String value = tw.value().toLowerCase();
						// Only keep regular nouns
						if (tag.equalsIgnoreCase("NN")
								&& !CommonDefectAnalyzerNew.hasStopWord(value))
							if (sentence.indexOf(tw) < sentence.size() - 1)
								buffer.append(value + "::");
							else
								buffer.append(value);
					}

					String nounStr = buffer.toString().trim();
					if (nounStr.length() > 0)// Skip empty lines
						pw2.println(nounStr);
				}

				num++;
			}
			pw1.close();
			pw2.close();

			// TopicSet topicSet =
			// TopicModel.extractTopics(threadFile.getAbsolutePath(), 10, 5);
			// topicSet.printDistribution();

			// Find the frequent patterns by the FP-Growth algorithm
			Map<String, Long> featureMap = findFrequenPatterns(threadNoun);
			Map<String, Long> cleanedFeatureMap = new HashMap<String, Long>();

			// Filter the incomplete properties
			for (String item : featureMap.keySet()) {
				if (!isIncompleteProperty(item))
					cleanedFeatureMap.put(item, featureMap.get(item));
			}
			printFrequentPatterns(cleanedFeatureMap);

			/*
			 * Set<String> featureSet = cleanedFeatureMap.keySet();
			 * 
			 * String manufacturer = CommonDefectAnalyzer.MANUFACTUER; String
			 * model = CommonDefectAnalyzer.MODEL;
			 * 
			 * int yearIdentified = 0; //Produce a table of posts which have the
			 * year and component of post for(Document doc: docList){ Date time
			 * = doc.getTime(); long postID = doc.getPostID(); int year = 0;
			 * year = TextUtil.extractYear(doc); if(year > 0){ //
			 * System.out.println("YEAR of MODEL: " + year); yearIdentified++; }
			 * 
			 * //Search the components in the specified document List<String>
			 * componentList = componentsInDocument(doc, featureSet);
			 * if(componentList.size() > 0) //Create a DB record for every
			 * component for(String component : componentList){ String strTime =
			 * (time == null) ? null : time.toString(); String sql =
			 * "insert into post_metrics values (NULL,'" + manufacturer + "', '"
			 * + model + "', " + year + ", '" + component + "', '" + strTime +
			 * "', " + postID + ")"; //db.executeUpdate(sql); } }
			 * 
			 * System.out.println("Year has been extracted from " +
			 * yearIdentified + "/" + num + " posts.");
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
