package console;

public class GlobalConstants {
	public static boolean DEBUG = true;
	public static final String POST_URL = "data/Camry3-4_Posts_NoHeader.csv";
	public static final String SMOKE_BIGRAM_URL = "dict/VehicleSmokeBigrams_NoHeader.csv";
	public static final String MODELS_URL = "config/ToyotaModels.txt";
	
	public static final int EARLEST_YEAR = 2002;
	public static final int THIS_YEAR = 2014;

	// Only posts posted in this range will be considered
	public static final int VALID_DISCUSSION_YEARS = 3;
	
	// Only count the times of recall happened in this scope
	public static final int RECALL_YEAR_SCOPE = 3;
	
	public static final int FEATURE_DIMENSION = 200;

	public static boolean isDEBUG() {
		return DEBUG;
	}

	public static void setDEBUG(boolean dEBUG) {
		DEBUG = dEBUG;
	}

}
