package console;

import java.io.File;
import java.util.Map;

import database.SQLCMD;
import utility.FileUtils;
///abcddd
public class DBReader {
	private static SQLCMD cmd = null;
		
	public static void main(String args[]){
		String configPath = "." + File.separator + "config" + File.separator + "config.properties";
		 
		Map<String, String> configMap = FileUtils.readKeyValueList(configPath);
		GlobalConstants.setDEBUG(Boolean.parseBoolean(configMap.get("DEBUG")));
		
		DBReader.cmd = new SQLCMD(configMap.get("DB_NAME"), 
				configMap.get("DB_HOST"), 
				Integer.parseInt(configMap.get("DB_PORT")), 
				configMap.get("DB_USER"), 
				configMap.get("DB_PASSWORD"));
	}

}
