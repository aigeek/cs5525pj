package console;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import preprocess.StanfordNLP;
import utility.FileUtils;
import utility.TextUtil;
import model.Document;
import model.PostRecord;
import database.QueryUtil;
import database.SQLCMD;
import feature.CategoryConstants;
import feature.ChiSquareSelector;
import feature.FeatureUtil;
import feature.InformationGainSelector;

public class RecallPredictNumerical {
	private static SQLCMD db = new SQLCMD("toyota", "128.173.49.48", 3306,
			"dapj", "cs5525");
	private static File threadNoun = new File("result/FinalResult.txt");
	private static PrintWriter pw = null;
	public static final int CLASS_NUM = 2;
	public static final int MINIMUM_OVERALL_FREQUENCY = 5;

	public static Map<String, Double> getTermFrequencies(String model,
			String year) {
		Map<String, Double> result = new HashMap<String, Double>();

		// Step 1 Search for the threads (their 1st posts) related with the
		// specified model and year

		// Step 2 Select features

		// Step 3 create a vector for this instance

		return result;
	}

	public static void analyze() {

	}

	public static List<PostRecord> getPosts(String model) {
		List<PostRecord> result = new ArrayList<PostRecord>();

		List<String> idList = new ArrayList<String>();
		String sql = "select id, forum_name from forums";
		List<Map<String, Object>> rows = db.executeQuery(sql);
		for (Map<String, Object> map : rows) {
			String id = (String) map.get("id");
			String forumName = (String) map.get("forum_name");
			forumName = forumName.toLowerCase();
			model = model.toLowerCase();
			if (forumName.indexOf(model) >= 0)
				idList.add(id);
		}

		for (String id : idList) {
			int forumID = Integer.parseInt(id);
			List<PostRecord> sublist = QueryUtil.extract1stPostsFromDB(db,
					forumID);
			result.addAll(sublist);
		}

		return result;
	}

	public static List<String> getModels() {
		List<String> result = FileUtils
				.readValueList(GlobalConstants.MODELS_URL);

		return result;
	}

	/**
	 * Change the map (key is the instance name, which is model and year; value
	 * is the titles and contents of all the related posts) to a document list
	 * 
	 * @param map
	 * @return
	 */
	private static List<Document> changeMapToList(Map<String, String> map) {
		List<Document> result = new ArrayList<Document>();

		int max = 0;
		int min = Integer.MAX_VALUE;

		// Create a document for each entry of map
		for (String key : map.keySet()) {
			String content = map.get(key);

			// TODO: For test
			System.out.println("\n*************************************");
			System.out.println(key);
			System.out.println(content);

			String[] array = key.split(":");
			String model = array[0];
			String year = array[1];

			int recallTimes = getRecallTimes(model, year);
			Document doc = new Document(key, content, null, recallTimes);
			if (recallTimes > max)
				max = recallTimes;
			if (recallTimes < min)
				min = recallTimes;
			result.add(doc);
		}

		// Scatter the recall times of each instance (model+year) to categories,
		// and set the value to the category attribute of the document
		for (Document doc : result) {
			int recallTimes = doc.getCategoryID();
			String category = convertNumToClass(recallTimes, max, min,
					CLASS_NUM);
			doc.setCategoryStr(category);
		}

		return result;
	}

	/**
	 * Lemmatize each document and remove stop word from them
	 * 
	 * @param docList
	 */
	public static void preprocess(List<Document> docList) {
		StanfordNLP slem = StanfordNLP.getInstance();
		for (Document doc : docList) {
			String text = doc.getContent();
			List<String> strs = slem.lemmatizeAndFilterStopWord(text, true,
					true);
			StringBuffer buffer = new StringBuffer();
			for (String str : strs) {
				buffer.append(str + " ");
			}
			text = buffer.toString();
			doc.setContent(text);
		}
	}

	/**
	 * Get the high frequency term set with a basic filtering
	 * 
	 * @param docList
	 * @return
	 */
	public static Set<String> getHighFrequencyTermSet(List<Document> docList) {
		Set<String> result = new TreeSet<String>();
		if (docList == null)
			return result;

		List<Document> smallList = new ArrayList<Document>();
		// For every 200 document, find the term set with high frequency, and
		// add this local set to the final set
		for (int i = 0; i < docList.size(); i++) {
			smallList.add(docList.get(i));
			if (i % 200 == 0 || i == docList.size() - 1) {
				Set<String> localTermSet = getLocalPopularTermSet(smallList);
				result.addAll(localTermSet);
				smallList = new ArrayList<Document>();
			}
		}

		System.err.println("Size of the High Frequency Term Set (TF>="
				+ MINIMUM_OVERALL_FREQUENCY + "): " + result.size());

		int num = 1;
		for (String str : result) {
			System.out.print(str + "\t");
			if (num % 5 == 0)
				System.out.println();
			num++;
		}
		System.out.println();

		return result;
	}

	/**
	 * Get the terms which are popular (frequent) in the specified document list
	 * 
	 * @param docList
	 * @return
	 */
	public static Set<String> getLocalPopularTermSet(List<Document> docList) {
		Set<String> result = new TreeSet<String>();
		Map<String, Integer> map = new TreeMap<String, Integer>();
		if (docList == null)
			return result;

		for (Document doc : docList) {
			String article = doc.getContent();
			// System.out.println(doc.getTitle());

			String[] array = article.split(" ");
			for (String str : array) {
				if (str.length() > 0) {
					Integer value = map.get(str);
					if (value == null)
						map.put(str, new Integer(1));
					else
						map.put(str, new Integer(value.intValue() + 1));
				}
			}
		}

		for (String str : map.keySet()) {
			Integer value = map.get(str);
			if (value.intValue() >= MINIMUM_OVERALL_FREQUENCY)
				result.add(str);
		}

		return result;
	}

	/**
	 * Convert the specified number (recall times) to a string class according
	 * to the specified number of classes and the scope of the number
	 * 
	 * @param num
	 * @param max
	 * @param min
	 * @param classNum
	 * @return
	 */
	public static String convertNumToClass(int num, int max, int min,
			int classNum) {
		int windowLength = (int) Math.round((double) (max - min) / classNum);
		int diff = num - min - 1;
		if (diff < 0)
			diff = 0;
		int transferedNum = diff / windowLength;

		String result = String.valueOf(transferedNum);
		System.out.println(result + "<-" + num + " [max: " + max + ", min: "
				+ min + "]");
		return result;
	}

	/**
	 * Get the recall time from the flat_rcl table according to the specified
	 * model and year
	 * 
	 * @param model
	 * @param year
	 * @return
	 */
	public static int getRecallTimes(String model, String year) {
		model = model.toUpperCase();
		String sql = "SELECT count(*) FROM flat_rcl " + "where MODELTXT like '"
				+ model + "' and " + "YEARTXT = '" + year + "'";

		List<Map<String, Object>> rows = db.executeQuery(sql);
		if (rows.size() < 1)
			return 0;
		else {
			Map<String, Object> map = rows.get(0);
			String value = (String) map.get(map.keySet().iterator().next());
			return Integer.parseInt(value);
		}

	}

	/**
	 * Create a term frequency array for every document in the specified list.
	 * The dimensions of the vector are specified by the termSet
	 * 
	 * @param docList
	 * @param termSet
	 */
	public static void createTermFrequencies(List<Document> docList,
			final Set<String> termSet, boolean freqPerYear) {
		if (docList == null || termSet == null) {
			System.err
					.println("Null pointer appears during creating term frequency for documents!");
			return;
		}

		String[] termArray = new String[termSet.size()];
		termArray = termSet.toArray(termArray);

		for (Document doc : docList) {
			// Initialize the term frequency array with zeros
			int[] tf = new int[termSet.size()];
			for (int i = 0; i < tf.length; i++) {
				tf[i] = 0;
			}

			String article = doc.getContent();
			String[] array = article.split(" ");

			// For every appearance of certain term, increase its frequency by 1
			for (String str : array) {
				if (str == null || str.length() == 0)
					continue;

				int index = TextUtil.binarySearch(termArray, str);
				// Skip the word which is not included in the feature
				if (index < 0)
					continue;
				// Increase the corresponding array element by 1
				tf[index] += 1;
			}

			// Calculate the term frequency per year if necessary
			if (freqPerYear) {
				String title = doc.getTitle();
				String strYear = title.split(":")[1];
				int modelYear = Integer.parseInt(strYear);
				int yearsLast = GlobalConstants.THIS_YEAR - modelYear + 1;
				int period = GlobalConstants.VALID_DISCUSSION_YEARS + 1;
				period = (period > yearsLast) ? yearsLast : period;

				for (int i = 0; i < tf.length; i++) {
					tf[i] = (int) Math.ceil(((double) tf[i] / period));
				}
			}

			// Set the term frequencies value of this document
			doc.setTermFrequencies(tf);
		}
	}

	public static int parseYear(String strYear) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM-dd-yyyy, hh:mm aa");
		Date time = null;
		try {
			time = dateFormat.parse(strYear);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

		return time.getYear() + 1900;
	}

	public static Map<String, String> extractInstanceMap(List<String> models) {
		Map<String, String> result = new HashMap<String, String>();
		for (String model : models) {
			System.out.println("\nGet posts of Model [" + model + "]...");
			List<PostRecord> postList = getPosts(model);
			for (PostRecord pr : postList) {
				Document doc = new Document(pr.getTitle(), pr.getContent(), 0);
				String strTime = pr.getTime();
				int publishYear = parseYear(strTime);
				int year = TextUtil.extractYear(doc);
				// Only keep the posts published within [ProductionYear-1,
				// ProductionYear+VALID_DISCUSSION_YEARS)
				if (year > GlobalConstants.EARLEST_YEAR
						&& year <= GlobalConstants.THIS_YEAR
						&& (publishYear < (year + GlobalConstants.VALID_DISCUSSION_YEARS) && (publishYear >= (year - 1)))) {
					String key = model + ":" + year;
					String body = result.get(key);
					if (body != null)
						body = body + "\n" + doc.getTitle() + "\n"
								+ doc.getContent();
					else
						body = doc.getTitle() + "\n" + doc.getContent();
					result.put(key, body);
				}
			}
		}

		return result;
	}

	public static Map<String, List<PostRecord>> extractThreadsOfModels(
			List<String> models) {
		Map<String, List<PostRecord>> result = new HashMap<String, List<PostRecord>>();
		for (String model : models) {
			System.out.println("\nGet posts of Model [" + model + "]...");
			List<PostRecord> postList = getPosts(model);
			for (PostRecord pr : postList) {
				Document doc = new Document(pr.getTitle(), pr.getContent(), 0);
				String strTime = pr.getTime();
				int publishYear = parseYear(strTime);
				int productYear = TextUtil.extractYear(doc);
				// Only keep the posts published within [ProductionYear-1,
				// ProductionYear+VALID_DISCUSSION_YEARS)
				if (productYear > GlobalConstants.EARLEST_YEAR
						&& productYear <= GlobalConstants.THIS_YEAR
						&& (publishYear < (productYear + GlobalConstants.VALID_DISCUSSION_YEARS) && (publishYear >= (productYear - 1)))) {
					String key = model + ":" + productYear;
					List<PostRecord> list = result.get(key);
					if (list != null)
						list.add(pr);
					else {
						list = new ArrayList<PostRecord>();
						list.add(pr);
					}
					result.put(key, list);
				}
			}
		}

		return result;
	}

	public static double getComponentNum(String model, String productYear) {
		return 0;
	}

	public static double getThreadsPerYear(String model, String productYear) {
		return 0;
	}

	public static double getUserEngagedPerYear(String model, String productYear) {
		return 0;
	}

	/**
	 * Get the recall time from the flat_rcl table according to the specified
	 * model and year
	 * 
	 * @param model
	 * @param productYear
	 * @return
	 */
	public static double getRecallTimesPerYear(String model, String productYear) {
		int recallTimes = 0;
		model = model.toUpperCase();
		String sql = "SELECT RCDATE FROM flat_rcl " + "where MODELTXT like '"
				+ model + "' and " + "YEARTXT = '" + productYear + "'";

		int iProductYear = Integer.parseInt(productYear);
		List<Map<String, Object>> rows = db.executeQuery(sql);
		if (rows.size() < 1)
			return 0;
		else {
			for (Map<String, Object> map : rows) {
				String strDate = (String) map.get(map.keySet().iterator()
						.next());
				System.out.println("Recall date: " + strDate);
				try {
					int recallYear = Integer.parseInt(strDate.substring(0, 4));
					if (recallYear >= (iProductYear - 1)
							&& recallYear < (iProductYear + GlobalConstants.RECALL_YEAR_SCOPE))
						recallTimes++;
				} catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
			}

		}

		int regularScope = GlobalConstants.RECALL_YEAR_SCOPE + 1;
		int smallScope = GlobalConstants.THIS_YEAR - iProductYear + 2;

		int years = (regularScope > smallScope) ? smallScope : regularScope;
		double result = ((double) recallTimes) / years;

		return result;

	}

	public static void testGetRecallTimesPerYear() {
		String model = "Camry";
		String year = "2007";
		double recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2008";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2009";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2010";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2011";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2012";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2013";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2014";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");

		year = "2015";
		recallTimesPerYear = getRecallTimesPerYear(model, year);
		System.out.println("Recall " + recallTimesPerYear
				+ " times per year for " + model + " " + year + "\n");
	}

	public static Map<String, Double> extractsThreadsPerYearMap(
			Map<String, List<PostRecord>> threadsOfModels) {
		Map<String, Double> threadsNumMap = new HashMap<String, Double>();
		for (String modelYear : threadsOfModels.keySet()) {
			int threads = 0;
			String model = modelYear.split(":")[0];
			String productYear = modelYear.split(":")[1];
			int iProductYear = Integer.parseInt(productYear);
			List<PostRecord> list = threadsOfModels.get(modelYear);
			for (PostRecord pr : list) {
				String strTime = pr.getTime();
				int publishYear = parseYear(strTime);
				if (publishYear >= (iProductYear - 1)
						&& publishYear < (iProductYear + GlobalConstants.VALID_DISCUSSION_YEARS))
					threads++;
			}

			int regularScope = GlobalConstants.VALID_DISCUSSION_YEARS + 1;
			int smallScope = GlobalConstants.THIS_YEAR - iProductYear + 2;

			int years = (regularScope > smallScope) ? smallScope : regularScope;
			double threadsPerYear = ((double) threads) / years;
			threadsNumMap.put(modelYear, new Double(threadsPerYear));
		}

		for (String modelYear : threadsNumMap.keySet()) {
			System.out.println(modelYear + "\t"
					+ threadsNumMap.get(modelYear).doubleValue());
		}

		return threadsNumMap;
	}

	public static Map<String, Double> getComponentsNumMap(
			Map<String, List<PostRecord>> threadsOfModels) {
		Map<String, Double> result = CommonDefectAnalyzerNew
				.getComponentNumMap(threadsOfModels);
		for (String modelYear : result.keySet()) {
			System.out.println(modelYear + "\t"
					+ result.get(modelYear).doubleValue());
			pw.println(modelYear + "\t"
					+ result.get(modelYear).doubleValue());
		}
		return result;
	}

	public static void main(String[] args) {

		try {
			pw = new PrintWriter(threadNoun);

			List<String> trainingModels = getModels();
			Map<String, List<PostRecord>> threadsOfModels = extractThreadsOfModels(trainingModels);

//			pw.println("Recall times per year:");
//			for (String modelYear : threadsOfModels.keySet()) {
//				String model = modelYear.split(":")[0];
//				String productYear = modelYear.split(":")[1];
//				double recallTimesPerYear = getRecallTimesPerYear(model,
//						productYear);
//				System.out.println("Recall " + recallTimesPerYear
//						+ " times per year for " + model + " " + productYear
//						+ "\n");
//				pw.println(modelYear + "\t" + recallTimesPerYear);
//			}
			
			pw.println("\nThreads of models per year:");
			for (String modelYear : threadsOfModels.keySet()) {
				pw.println(modelYear + "\t" + threadsOfModels.get(modelYear).size());
			}

			pw.println("\nComponents of models per year:");
			getComponentsNumMap(threadsOfModels);
			// testGetRecallTimesPerYear();
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
