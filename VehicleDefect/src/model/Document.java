package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Document implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String title;

	private String content;
	
	private double metrics;

	private Date time;

	private String company;

	private int categoryID;

	private String categoryStr;

	private int[] termFrequencies;
	
	private long postID;


	public Document(String title, String content, Date date, String company) {
		super();
		this.title = title;
		this.content = content;
		this.time = date;
		this.company = company;
	}

	public Document(String title, String content, String company,
			String categoryStr) {
		this.title = title;
		this.content = content;
		this.company = company;
		this.categoryStr = categoryStr;
	}
	
	public Document(String title, String content, double metrics) {
		this.title = title;
		this.content = content;
		this.metrics = metrics;
	}
	
	public Document(String title, String content, double metrics, Date time, long postID) {
		this.title = title;
		this.content = content;
		this.metrics = metrics;
		this.time = time;
		this.postID = postID;
	}
	
	public Document(String title, String content, String company,
			int categoryID) {
		this.title = title;
		this.content = content;
		this.company = company;
		this.categoryID = categoryID;
	}

	public Document(String title, String content, Date date, String company,
			int categoryID, String categoryStr, int[] termFrequencies) {
		super();
		this.title = title;
		this.content = content;
		this.time = date;
		this.company = company;
		this.categoryID = categoryID;
		this.categoryStr = categoryStr;
		this.termFrequencies = termFrequencies;
	}

	public String getCategoryStr() {
		return categoryStr;
	}

	public void setCategoryStr(String categoryStr) {
		this.categoryStr = categoryStr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public double getMetrics() {
		return metrics;
	}

	public void setMetrics(double metrics) {
		this.metrics = metrics;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date date) {
		this.time = date;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public int[] getTermFrequencies() {
		return termFrequencies;
	}

	public void setTermFrequencies(int[] termFrequencies) {
		this.termFrequencies = termFrequencies;
	}
	
	public long getPostID() {
		return postID;
	}

	public void setPostID(long postID) {
		this.postID = postID;
	}

	public Document clone() {
		// Copy the term frequency by value
		int[] tfCopy = new int[this.termFrequencies.length];
		for (int i=0;i<this.termFrequencies.length;i++){
			tfCopy[i] = this.termFrequencies[i];
		}
		//System.arraycopy(this.termFrequencies, 0, tfCopy, 0, this.termFrequencies.length);
		
		return new Document(this.title, this.content, this.time, this.company,
				this.categoryID, this.categoryStr, 
				tfCopy);
	}
	
	public static List<Document> cloneDocumentList(final List<Document> list){
		synchronized (list){
			List<Document> result = new ArrayList<Document>();
			if(list == null){
				System.err.println("NULL document list!");
				return null;
			}
			
			for(Document item : list){
				result.add(item.clone());
			}
			
			return result;
		}
	}

}
