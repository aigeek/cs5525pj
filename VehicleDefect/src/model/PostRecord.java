package model;

public class PostRecord {

	private String title;
	private String time;
	private String content;
	private String id;
	

	public PostRecord(String title, String time, String content, String id) {
		super();
		this.title = title;
		this.time = time;
		this.content = content;
		this.id = id;
	}
	
	public PostRecord(){
		this.title = null;
		this.time = null;
		this.content = null;
		this.id = null;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
