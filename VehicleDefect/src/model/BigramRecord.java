package model;

public class BigramRecord {
	private String bigram;
	private double rsv;
	
	public BigramRecord(String bigram, double rsv) {
		super();
		this.bigram = bigram;
		this.rsv = rsv;
	}
	
	public BigramRecord(){
		this.bigram = null;
		this.rsv = 0;
	}
	
	
	public String getBigram() {
		return bigram;
	}
	public void setBigram(String bigram) {
		this.bigram = bigram;
	}
	public double getRsv() {
		return rsv;
	}
	public void setRsv(double rsv) {
		this.rsv = rsv;
	}
	
	
	

}
