package fpmin;
import java.util.ArrayList;
import java.util.List;

public class FpNode {

	String idName;
	List<FpNode> children;
	FpNode parent;
	FpNode next;
	long count;

	public FpNode() {
		this.idName = null;
		this.count = -1;
		children = new ArrayList<FpNode>();
		next = null;
		parent = null;
	}

	/**
	 * 
	 * @param idName
	 * @param count
	 */
	public FpNode(String idName) {
		this.idName = idName;
		this.count = 1;
		children = new ArrayList<FpNode>();
		next = null;
		parent = null;
	}

	/**
	 * 
	 * @param idName
	 * @param count
	 */
	public FpNode(String idName, long count) {
		this.idName = idName;
		this.count = count;
		children = new ArrayList<FpNode>();
		next = null;
		parent = null;
	}
	
	public String getIdName() {
		return idName;
	}

	public void setIdName(String idName) {
		this.idName = idName;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	/**
	 * 
	 * @param child
	 */
	public void addChild(FpNode child) {
		children.add(child);
	}

	public void addCount(int count) {
		this.count += count;
	}

	/**
	 */
	public void addCount() {
		this.count += 1;
	}

	/**
	 * 
	 * @param next
	 */
	public void setNextNode(FpNode next) {
		this.next = next;
	}

	public void setParent(FpNode parent) {
		this.parent = parent;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public FpNode getChilde(int index) {
		return children.get(index);
	}

	/**
	 * 
	 * @param idName
	 * @return
	 */
	public int hasChild(String idName) {
		for (int i = 0; i < children.size(); i++)
			if (children.get(i).idName.equals(idName))
				return i;
		return -1;
	}

	public String toString() {
		return "id: " + idName + " count: " + count + " size"
				+ children.size();
	}
	
	public List<FpNode> getChildren() {
		return children;
	}

	public void setChildren(List<FpNode> children) {
		this.children = children;
	}

	public FpNode getNext() {
		return next;
	}

	public void setNext(FpNode next) {
		this.next = next;
	}

	public FpNode getParent() {
		return parent;
	}
}
