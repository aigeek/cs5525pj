package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import preprocess.StanfordNLP;
import utility.OSCheck.OSType;

public class FileUtils {
	public static Map<String, String> readKeyValueList(String location) {
		Map<String, String> result = new ConcurrentHashMap<String, String>();
		if (location == null || location.length() <= 0) {
			System.err.println("NULL configuration file!");
			return result;
		}

		try {
			File file = new File(location);
			if(!file.exists()){
				
				System.err.println("Failed to read configuration! Please put the configuration file under the path: " + location);
				System.err.println("Wrong path: " + file.getPath());
				System.exit(0);
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while (line != null) {
				if (line != null && line.indexOf(':') > 0) {
					String[] array = line.split(":", 2);
					result.put(array[0].trim(), array[1].trim());
				}
				line = br.readLine();
			}

			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	public static List<String> readValueList(String location) {
		List<String> result = new ArrayList<String>();
		if (location == null || location.length() <= 0) {
			System.err.println("NULL configuration file!");
			return result;
		}

		try {
			File file = new File(location);
			if(!file.exists()){
				
				System.err.println("Failed to read configuration! Please put the configuration file under the path: " + location);
				System.err.println("Wrong path: " + file.getPath());
				System.exit(0);
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while (line != null) {
				if (line != null && line.indexOf(':') > 0) {
					String[] array = line.split(":", 2);
					result.add(array[1].trim());
				}
				line = br.readLine();
			}

			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	public static void findAllTextFiles(File dir, List<File> list) {
		if (dir == null || !dir.isDirectory())
			return;
		File[] array = dir.listFiles();
		if (array == null || array.length == 0)
			return;
		for (File item : array) {
			if (item.isFile()) {
				String fileName = item.getName();
				item.getAbsolutePath();
				if (fileName != null && fileName.endsWith(".txt"))
					list.add(item);
			} else
				findAllTextFiles(item, list);
		}
	}
	
	public static void splitDocumentsIntoParagraphs(String sourceDir,
			String outputDir, boolean stemming, int skipTopLines) {
		if (sourceDir == null || outputDir == null)
			return;

		preprocess.StanfordNLP slem = StanfordNLP.getInstance();

		File outputFile = new File(outputDir);
		boolean flag = false;
		if (!outputFile.exists()) {
			flag = outputFile.mkdirs();
			if (!flag) {
				System.err.println("Failed to create output directory!");
				return;
			}
		}

		File sourceDirFile = new File(sourceDir);
		List<File> list = new ArrayList<File>();
		findAllTextFiles(sourceDirFile, list);

		for (File file : list) {
			String title = file.getName();
			String content = readTextFromFile(file);
			Date date = getNewsDate(file);
			String strDate = dateToString(date);
			String[] array = content.split("\n");

			//If there're too few lines in the document, skip it
			if(array.length <= skipTopLines)
				continue;
			
			int count = 1;
			for (String text : array) {
				if (text.trim().length() == 0)
					continue;
				
				//Skip the top N lines (usually the top 3 lines are the topic, time and agency of the news)
				if(count <= skipTopLines){
					count++;
					continue;
				}
				
				// the name of the file is like title-001.txt
				String paraFileUrl = outputDir + File.separator + title + "-"
						+ String.format("%03d", count) + ".txt";

				if(stemming){
					// Do stemming to the text before writing them to files.
					StringBuffer buffer = new StringBuffer();
					if(strDate != null)//Append the date information to the paragraph
						buffer.append("Date: " + strDate + ".. ");
					List<String> strs = slem.lemmatizeAndFilterStopWord(text, true,
							true);
					for (String item : strs) {
						buffer.append(item + " ");
					}

					printTextToFile(buffer.toString().trim(), paraFileUrl);
	
				}else{
					if(strDate != null)//Append the date information to the paragraph
						text = "Date: " + strDate + ".. " + text;
					printTextToFile(text, paraFileUrl);
				}
				
				count++;
			}

		}
	}
	
	/**
	 * Extract the news creation date according to its path in the file system
	 * e.g.D:\Test\MH370\2014\3\8\Angry Chinese families want more information on missing Malaysia Airlines flight.txt
	 * @param file
	 * @return
	 */
	public static Date getNewsDate(File file){
		if(file == null || !file.exists() || file.isDirectory())
			return null;
		
		String path = file.getAbsolutePath();
		OSType osType = OSCheck.getOperatingSystemType();
		String[] array;
		
		if(osType == OSType.Windows)
			array = path.split("\\\\");
		else
			array = path.split("/");
		int length = array.length;
		
		Calendar calendar = Calendar.getInstance();
		try{
			int day = Integer.parseInt(array[length-2]);
			int month = Integer.parseInt(array[length-3]);
			int year = Integer.parseInt(array[length-4]);
			
			calendar.set(year, month-1, day);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
		return calendar.getTime();
	}
	
	public static String dateToString(Date date){
		if(date == null)
			return null;
		
		DateFormat df = DateFormat.getDateInstance();
		return df.format(date);
	}
	
	public static void printTextToFile(String text, String url) {
		if (text == null || url == null)
			return;

		try {
			PrintWriter pw = new PrintWriter(url);
			pw.print(text);
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String readTextFromFile(File file) {
		StringBuffer buffer = new StringBuffer();
		
		char[] array = new char[4*1024];
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			int num = br.read(array);
			
			while (num > 0) {
				buffer.append(array);
				array = new char[4*1024];
				num = br.read(array);
			}
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
			return buffer.toString();
		}

		return buffer.toString();
	}

}
