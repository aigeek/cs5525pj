package utility;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class CSVParser {
	public static List<Object> retrieveDataFromCSV(String url, Class classType, String[] columns){
		List<Object> result = null;
		ColumnPositionMappingStrategy strat = new ColumnPositionMappingStrategy();
		strat.setType(classType);
		// the fields to bind do in your JavaBean
		//String[] columns = new String[] {"predicted", "p0", "p1", "merchant", "e1", "e2", "id"}; 
		strat.setColumnMapping(columns);

		CsvToBean csv = new CsvToBean();
		try {
			result = (List<Object>)csv.parse(strat, new CSVReader(new FileReader(url)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n" + result.size() + " records parsed from " + url + "\n");
		return result;
	}

}
