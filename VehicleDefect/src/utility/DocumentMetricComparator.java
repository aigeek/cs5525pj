package utility;

import java.util.Comparator;

import model.Document;

public class DocumentMetricComparator<T> implements Comparator<T> {

	@Override
	public int compare(T c1, T c2) {

		if (!(c1 instanceof Document) || !(c2 instanceof Document)) {
			System.err.println("Either of the two objects is NOT Document!");
			return 0;
		}

		double metric1 = ((Document) c1).getMetrics();
		double metric2 = ((Document) c2).getMetrics();

		// Avoid testing the equality of two doubles
		if (metric1 > metric2)
			return 1;
		else if (metric1 < metric2)
			return -1;
		
		long lMetric1 = Double.doubleToLongBits(metric1);
		long lMetric2 = Double.doubleToLongBits(metric2);
		
		return (lMetric1 == lMetric2 ? 0 : (lMetric1 < lMetric2 ? -1 : 1));

	}

}
