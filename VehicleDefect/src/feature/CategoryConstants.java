package feature;

public class CategoryConstants {
	public static final int[] CATEGORY_IDS = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
			11 };
	//categories of the twitter data 
	//public static final String [] CATEGORY_STRINGS = {"A", "E", "G", "H", "I", "K"};
	
	//categories of the S&P 500 data (adverse or non-adverse event)
	public static final String [] CATEGORY_STRINGS = {"Y", "N"};
	
	public static final String[] CATEGORY_NAMES = { "New customers",
			"New Products/Services", "Promotions", "Stakeholder Actions",
			"Alliances", "Acquisitions", "Internationalization",
			"Management team building", "Organizational changes",
			"Legal and lobbying", "None" };
	
	public static final int CATEGORY_CATEGORICAL = 1;
	public static final int CATEGORY_NUMERICAL = 2;

	public static String getCategoryNameByID(int i){
		if (i < 1 || i > 11)
			return null;
		return CATEGORY_NAMES[i-1];
	}
}
