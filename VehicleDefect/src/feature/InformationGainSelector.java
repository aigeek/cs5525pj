package feature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import console.GlobalConstants;
import utility.MathUtil;
import utility.TermMetricComparator;
import utility.TextUtil;
import model.Document;
import model.TermMetric;

public class InformationGainSelector implements FeatureSelector {

	@Override
	public List<String> selectFeatures(List<Document> documents,
			Set<String> fullTermSet, int featureNum, int categoryType) {
		List<String> result = new ArrayList<String>();

		List<TermMetric> termIGList = new ArrayList<TermMetric>();
		String[] termArray = new String[fullTermSet.size()];
		termArray = fullTermSet.toArray(termArray);

		int num = 1;
		for (String str : fullTermSet) {
			double ig = this.computeInformationGain(str, documents, termArray,
					categoryType);
			termIGList.add(new TermMetric(str, ig));
			System.out.println("The IG of term[" + num + "] <" + str + "> is "
					+ ig);
			num++;
		}

		// System.err.println("The size of document list is " +
		// MemoryMeasurer.measureBytes(documents));

		// Sort the term list according to the IG of the term
		Collections.sort(termIGList, new TermMetricComparator<TermMetric>());

		// Return the top FEATURE_NUMBER of terms with the highest IG
		for (int i = 1; i <= GlobalConstants.FEATURE_DIMENSION; i++) {
			int index = termIGList.size() - i;

			if (index >= 0) {
				TermMetric tm = termIGList.get(index);
				String feature = tm.getTerm();
				double metric = tm.getMetric();
				System.err.println("** " + feature + " : " + metric);
				result.add(feature);
			} else
				break;

		}

		return result;
	}

	/**
	 * Compute the information gain value of the specified term from the given
	 * documents
	 * 
	 * @param term
	 * @param documents
	 * @return
	 */
	public double computeInformationGain(String term,
			final List<Document> documents, final String[] termArray,
			int categoryType) {
		double result = 0;
		double sum1 = 0, sum2 = 0, sum3 = 0;
		double termProbability = 0;

		termProbability = this.computeProbabilityOfTerm(term, termArray,
				documents);

		if (categoryType == CategoryConstants.CATEGORY_NUMERICAL) {
			for (int categoryID : CategoryConstants.CATEGORY_IDS) {
				double categoryPr = this.computeProbabilityOfCategory(
						categoryID, documents);
				sum1 += categoryPr * MathUtil.log(categoryPr, 2.0);

				double posConditionalCategoryPr = this
						.computeConditionalProbabilityOfCategory(term, true,
								categoryID, termArray, documents);
				sum2 += posConditionalCategoryPr
						* MathUtil.log(posConditionalCategoryPr, 2.0);

				double negConditionalCategoryPr = this
						.computeConditionalProbabilityOfCategory(term, false,
								categoryID, termArray, documents);
				sum3 += negConditionalCategoryPr
						* MathUtil.log(negConditionalCategoryPr, 2.0);
			}
		} else if (categoryType == CategoryConstants.CATEGORY_CATEGORICAL) {
			String[] categories = getCategorySet(documents);
			for (String categoryStr : categories) {
				double categoryPr = this.computeProbabilityOfCategory(
						categoryStr, documents);
				sum1 += categoryPr * MathUtil.log(categoryPr, 2.0);

				double posConditionalCategoryPr = this
						.computeConditionalProbabilityOfCategory(term, true,
								categoryStr, termArray, documents);
				sum2 += posConditionalCategoryPr
						* MathUtil.log(posConditionalCategoryPr, 2.0);

				double negConditionalCategoryPr = this
						.computeConditionalProbabilityOfCategory(term, false,
								categoryStr, termArray, documents);
				sum3 += negConditionalCategoryPr
						* MathUtil.log(negConditionalCategoryPr, 2.0);
			}

		}

		result = -sum1 + termProbability * sum2 + (1 - termProbability) * sum3;

		// System.err.println("Term[" + term + "] : " + result);

		return result;
	}

	private String[] getCategorySet(List<Document> documents) {
		Set<String> set = new HashSet<String>();
		for(Document doc : documents){
			String category = doc.getCategoryStr();
			if(category != null)
				set.add(category);
		}
		
		String[] array = new String[set.size()];
		array = set.toArray(array);
		return array;
	}

	/**
	 * Compute the information gain value of the specified term from the given
	 * documents
	 * 
	 * @param term
	 * @param documents
	 * @return
	 */
	public double computeInformationGain(String term,
			final List<Document> documents, final String[] termArray,
			String[] categories) {
		double result = 0;
		double sum1 = 0, sum2 = 0, sum3 = 0;
		double termProbability = 0;

		termProbability = this.computeProbabilityOfTerm(term, termArray,
				documents);

		for (String categoryStr : categories) {
			double categoryPr = this.computeProbabilityOfCategory(categoryStr,
					documents);
			sum1 += categoryPr * MathUtil.log(categoryPr, 2.0);

			double posConditionalCategoryPr = this
					.computeConditionalProbabilityOfCategory(term, true,
							categoryStr, termArray, documents);
			sum2 += posConditionalCategoryPr
					* MathUtil.log(posConditionalCategoryPr, 2.0);

			double negConditionalCategoryPr = this
					.computeConditionalProbabilityOfCategory(term, false,
							categoryStr, termArray, documents);
			sum3 += negConditionalCategoryPr
					* MathUtil.log(negConditionalCategoryPr, 2.0);
		}

		result = -sum1 + termProbability * sum2 + (1 - termProbability) * sum3;

		// System.err.println("Term[" + term + "] : " + result);

		return result;
	}

	/**
	 * Calculate the probability of certain category, which is in fact
	 * calculated as: (number of documents labeled as the specified category) /
	 * (total number of documents)
	 * 
	 * @param categoryID
	 * @param documents
	 * @return
	 */
	public double computeProbabilityOfCategory(int categoryID,
			List<Document> documents) {
		int hitNum = 0;
		for (Document doc : documents) {
			int categoryOfDoc = doc.getCategoryID();
			if (categoryOfDoc == categoryID)
				hitNum++;
		}

		int total = documents.size();
		return (double) hitNum / total;
	}

	/**
	 * Calculate the probability of certain category, which is in fact
	 * calculated as: (number of documents labeled as the specified category) /
	 * (total number of documents)
	 * 
	 * @param categoryStr
	 * @param documents
	 * @return
	 */
	public double computeProbabilityOfCategory(String categoryStr,
			List<Document> documents) {
		int hitNum = 0;
		for (Document doc : documents) {
			String categoryOfDoc = doc.getCategoryStr();
			if (categoryOfDoc != null && categoryOfDoc.equals(categoryStr))
				hitNum++;
		}

		int total = documents.size();
		return (double) hitNum / total;
	}

	/**
	 * Calculate the probability of the specified term from the documents, which
	 * is in fact calculated as: (number of documents which contains the term) /
	 * (total number of documents)
	 * 
	 * @param term
	 * @param documents
	 * @return
	 */
	public double computeProbabilityOfTerm(String term,
			final String[] termArray, final List<Document> documents) {
		int hitNum = 0;
		for (Document doc : documents) {
			int[] termFrequencies = doc.getTermFrequencies();
			int index = TextUtil.binarySearch(termArray, term);
			int termFrequency = termFrequencies[index];
			// If term appears in this document, increase the hitNum by 1
			if (termFrequency > 0)
				hitNum++;
		}

		int total = documents.size();
		return (double) hitNum / total;
	}

	/**
	 * Calculate the conditional probability of the specified category given the
	 * specified term, which is calculated by: (number of document which belong
	 * to specified category and contains specified term) / (number of document
	 * which contains specified term) OR (number of document which belong to
	 * specified category and not contains specified term) / (number of document
	 * which not contains specified term) The method to be picked depends on the
	 * value of "exist"
	 * 
	 * @param term
	 * @param exist
	 * @param categoryID
	 * @param documents
	 * @return
	 */
	public double computeConditionalProbabilityOfCategory(String term,
			boolean exist, int categoryID, final String[] termArray,
			final List<Document> documents) {
		// Two values which depend on the exist parameter:
		// 1. the number of document which belongs to the specified category and
		// contains the specified term
		// 2. the number of document which belongs to the specified category and
		// not contains the specified term
		int hitNum = 0;

		// the number of document which contains the specified term
		int termAppearNum = 0;

		for (Document doc : documents) {
			int categoryOfDoc = doc.getCategoryID();
			int[] termFrequencies = doc.getTermFrequencies();
			int index = TextUtil.binarySearch(termArray, term);
			int termFrequency = termFrequencies[index];

			if (termFrequency > 0)
				termAppearNum++;

			// Filter the documents which not belong to the specified category,
			// only take account the specified category of documents
			if (categoryOfDoc != categoryID)
				continue;

			if (exist)
				// If term appears in this document, increase the hitNum by 1
				if (termFrequency > 0)
					hitNum++;
			if (!exist)// If term not appears in this document, increase the
						// hitNum by 1
				if (termFrequency == 0)
					hitNum++;
		}

		// Choose the documents which contains or not contains the specified
		// term
		int divisor = exist ? termAppearNum
				: (documents.size() - termAppearNum);

		return (double) hitNum / divisor;
	}

	/**
	 * Calculate the conditional probability of the specified category given the
	 * specified term, which is calculated by: (number of document which belong
	 * to specified category and contains specified term) / (number of document
	 * which contains specified term) OR (number of document which belong to
	 * specified category and not contains specified term) / (number of document
	 * which not contains specified term) The method to be picked depends on the
	 * value of "exist"
	 * 
	 * @param term
	 * @param exist
	 * @param categoryID
	 * @param documents
	 * @return
	 */
	public double computeConditionalProbabilityOfCategory(String term,
			boolean exist, String categoryStr, final String[] termArray,
			final List<Document> documents) {
		// Two values which depend on the exist parameter:
		// 1. the number of document which belongs to the specified category and
		// contains the specified term
		// 2. the number of document which belongs to the specified category and
		// not contains the specified term
		int hitNum = 0;

		// the number of document which contains the specified term
		int termAppearNum = 0;

		for (Document doc : documents) {
			String categoryOfDoc = doc.getCategoryStr();
			int[] termFrequencies = doc.getTermFrequencies();
			int index = TextUtil.binarySearch(termArray, term);
			int termFrequency = termFrequencies[index];

			if (termFrequency > 0)
				termAppearNum++;

			// Filter the documents which not belong to the specified category,
			// only take account the specified category of documents
			if (categoryOfDoc != null && !categoryOfDoc.equals(categoryStr))
				continue;

			if (exist)
				// If term appears in this document, increase the hitNum by 1
				if (termFrequency > 0)
					hitNum++;
			if (!exist)// If term not appears in this document, increase the
						// hitNum by 1
				if (termFrequency == 0)
					hitNum++;
		}

		// Choose the documents which contains or not contains the specified
		// term
		int divisor = exist ? termAppearNum
				: (documents.size() - termAppearNum);

		return (double) hitNum / divisor;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> selectFeaturesForClass(List<Document> documents,
			Set<String> fullTermSet, int featureNum, String strClass) {
		// TODO Auto-generated method stub
		return null;
	}

}
