package feature;

import java.util.List;
import java.util.Set;

import model.Document;



public interface FeatureSelector {
	
	/**
	 * select the specified number of features out of the full term set
	 * 
	 * @param documents		-	the entire document list
	 * @param fullTermSet	-	the full term set
	 * @param featureNum	-	the number of desired features
	 * @param categoryType	-	category type: numerical or nominal (categorical)	
	 * @return
	 */
	public List<String> selectFeatures(List<Document> documents, Set<String> fullTermSet, 
			int featureNum, int categoryType);

	/**
	 * select the specified number of features out of the full term set
	 * 
	 * @param documents		-	the entire document list
	 * @param fullTermSet	-	the full term set
	 * @param featureNum	-	the number of desired features
	 * @param strClass		-	the index of the specified class
	 * @return
	 */
	public List<String> selectFeaturesForClass(List<Document> documents,
			Set<String> fullTermSet, int featureNum, String strClass);

}
