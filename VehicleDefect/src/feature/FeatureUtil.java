package feature;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import model.Document;

public class FeatureUtil {
	/**
	 * Export the features (Term Frequency) of the specified documents to the
	 * specified URL
	 * 
	 * @param docList
	 * @param url
	 */
	public static void exportFeatureToCSV(List<Document> docList, String url,
			final Set<String> featureSet, int categoryType) {
		File file = new File(url);
		String[] featureArray = new String[featureSet.size()];
		featureArray = featureSet.toArray(featureArray);

		try {
			PrintWriter pw = new PrintWriter(file);

			int docNum = 0;
			for (Document doc : docList) {
				int categoryID = 0;
				String categoryStr = null;

				// Map<String, Integer> map = doc.getVector();
				if (categoryType == CategoryConstants.CATEGORY_CATEGORICAL)
					categoryStr = doc.getCategoryStr();
				else if (categoryType == CategoryConstants.CATEGORY_NUMERICAL)
					categoryID = doc.getCategoryID();

				// Print the CSV head
				if (docNum == 0) {
					pw.print("CategoryID,");
					int termNum = 0;
					for (String key : featureArray) {
						pw.print(key);
						// Do not print comma at the end of the line
						if (termNum < featureArray.length - 1)
							pw.print(",");
						termNum++;
					}
					pw.println();
				}

				// Print every row (for every document)
				if (categoryType == CategoryConstants.CATEGORY_CATEGORICAL)
					pw.print(categoryStr + ",");
				else if (categoryType == CategoryConstants.CATEGORY_NUMERICAL)
					pw.print(categoryID + ",");

				int termNum = 0;
				int[] tfs = doc.getTermFrequencies();
				for (int i = 0; i < tfs.length; i++) {
					int value = tfs[i];
					pw.print(value);
					// Do not print comma at the end of the line
					if (termNum < tfs.length - 1)
						pw.print(",");
					termNum++;
				}
				pw.println();
				docNum++;
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Export the features (Term Frequency) of the specified documents to the
	 * specified URL
	 * 
	 * @param docList
	 * @param url
	 */
	public static void exportFeatureToCSV(List<Document> docList, String url,
			final Set<String> featureSet, int categoryType, 
			boolean binaryClasses, String strClass) {
		File file = new File(url);
		String[] featureArray = new String[featureSet.size()];
		featureArray = featureSet.toArray(featureArray);

		try {
			PrintWriter pw = new PrintWriter(file);

			int docNum = 0;
			for (Document doc : docList) {
				int categoryID = 0;
				String categoryStr = null;

//				Map<String, Integer> map = doc.getVector();
				if (categoryType == CategoryConstants.CATEGORY_CATEGORICAL)
					categoryStr = doc.getCategoryStr();
				else if (categoryType == CategoryConstants.CATEGORY_NUMERICAL)
					categoryID = doc.getCategoryID();

				// Print the CSV head
				if (docNum == 0) {
					//TODO: For test only
					pw.print("InstanceName,");
					pw.print("InstanceTruth,");
					
					pw.print("CategoryID,");
					int termNum = 0;
					for (String key : featureArray) {
						pw.print(key);
						// Do not print comma at the end of the line
						if (termNum < featureArray.length - 1)
							pw.print(",");
						termNum++;
					}
					pw.println();
				}
				
				//TODO: For test only
				pw.print(doc.getTitle() + ",");
				pw.print(doc.getCategoryID() + ",");
				
				//Convert the multiple classes into binary classes:
				//For binary-classification purpose (with multiple classes), set the multiple class values to "Y" or "N"
				//For multiple-classification purpose, never change the class name
				if(binaryClasses){
					if(categoryStr.equals(strClass))
						categoryStr = "Y";
					else
						categoryStr = "N";
				}

				// Print every row (for every document)
				if (categoryType == CategoryConstants.CATEGORY_CATEGORICAL)
					pw.print(categoryStr + ",");
				else if (categoryType == CategoryConstants.CATEGORY_NUMERICAL)
					pw.print(categoryID + ",");

				int termNum = 0;
				int[] tfs = doc.getTermFrequencies();
				for (int i=0;i<tfs.length;i++) {
					int value = tfs[i];
					pw.print(value);
					// Do not print comma at the end of the line
					if (termNum < tfs.length - 1)
						pw.print(",");
					termNum++;
				}
				pw.println();
				docNum++;
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Select features for every class by the specified feature selection algorithm
	 * 
	 * @param fullTermSet
	 * @param documents
	 * @return
	 */
	public static Map<String, List<String>> selectFeatures(FeatureSelector selector, 
			Set<String> fullTermSet, int featureNum,  
			final List<Document> documents){
		Map<String, List<String>> result = new TreeMap<String,List<String>>();
		for(String strClass : CategoryConstants.CATEGORY_STRINGS){
			List<String> list = selector.selectFeaturesForClass(documents, fullTermSet, featureNum, strClass);
			result.put(strClass, list);
		}
		
		return result;
		
	}
	
	public static FeatureSelector getFeatureSelector(String name){
		if(name == null)
			return null;
		else if(name.equalsIgnoreCase("Chi-Square"))
			return new ChiSquareSelector();
		else if(name.equalsIgnoreCase("IG"))
			return new InformationGainSelector();
		return null;
	}

}
