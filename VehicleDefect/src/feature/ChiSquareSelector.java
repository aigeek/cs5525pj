package feature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import utility.TermMetricComparator;
import utility.TextUtil;
import model.Document;
import model.TermMetric;


public class ChiSquareSelector implements FeatureSelector {

	@Override
	public List<String> selectFeatures(List<Document> documents,
			Set<String> fullTermSet, int featureNum, int categoryType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> selectFeaturesForClass(List<Document> documents,
			Set<String> fullTermSet, int featureNum, String strClass) {
		if (documents == null || fullTermSet == null || strClass == null) {
			System.err
					.println("NULL pointer when selecting features by Chi-Square algorithm");
			return null;
		}

		List<String> result = new ArrayList<String>();

		synchronized (fullTermSet) {
			List<TermMetric> termCSList = new ArrayList<TermMetric>();
			String[] termArray = new String[fullTermSet.size()];
			termArray = fullTermSet.toArray(termArray);

			for (String str : fullTermSet) {
				double cs = this.computeChiSquare(str, documents, termArray,
						strClass);
				termCSList.add(new TermMetric(str, cs));
			}

			// Sort the term list according to the CS of the term
			Collections
					.sort(termCSList, new TermMetricComparator<TermMetric>());

			System.out.println("\n\nSelect " + featureNum
					+ " features for class <" + strClass + ">:");

			// Return the top FEATURE_NUMBER of terms with the highest CS
			for (int i = 1; i <= featureNum; i++) {
				int index = termCSList.size() - i;

				if (index >= 0) {
					TermMetric tm = termCSList.get(index);
					String feature = tm.getTerm();
					double metric = tm.getMetric();
					System.out.println("** " + feature + " : " + metric);
					result.add(feature);
				} else
					break;
			}
		}

		return result;
	}

	private double computeChiSquare(String term,
			final List<Document> documents, final String[] termArray,
			String strClass) {
		double cs = 0;

		if (term == null || documents == null || termArray == null
				|| strClass == null) {
			System.err
					.println("NULL pointer when computing the chi-square value of term "
							+ term);
			return 0;
		}

		// Number of documents that contain the term and belong to the specified
		// class
		double A = 0;
		// Number of documents that contain the term but not belong to the
		// specified class
		double B = 0;
		// Number of documents that not contain the term but belong to the
		// specified class
		double C = 0;
		// Number of documents that not contain the term and not belong to the
		// specified class
		double D = 0;

		for (Document doc : documents) {
			int[] termFrequencies = doc.getTermFrequencies();
			String labeledClass = doc.getCategoryStr();
			int index = TextUtil.binarySearch(termArray, term);
			if(index >= termFrequencies.length || index < 0){
				System.err.println("Failed to find <" + term + "> in the term frequency array");
				continue;
			}
			int termFrequency = termFrequencies[index];

			if (termFrequency > 0) {
				if (strClass.equals(labeledClass))
					A++;
				else
					B++;
			} else {
				if (strClass.equals(labeledClass))
					C++;
				else
					D++;
			}
		}

		cs = Math.pow(A * D - B * C, 2) / ((A + B) * (C + D));
		return cs;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
