package database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DBStatistics {
	public static List<String> getForumStatistics(String curDbName, String curDbHost, 
			int curDbPort, String curDbUser, String curDbPassword){
		List<String> result = new ArrayList<String>();
		SQLCMD cmd = new SQLCMD(curDbName, curDbHost, curDbPort, curDbUser, curDbPassword);
		List<Map<String, Object>> list = cmd.executeQuery("SELECT id, forum_name from forums");
		for(Map<String, Object> map : list){
			String forumId = (String)map.get("id");
			String forum_name = (String)map.get("forum_name");
			
			List<Map<String, Object>> rs1 = cmd.executeQuery("SELECT count(*) FROM thread WHERE thread.forum_id = " + forumId);
			String threadNum = (String)rs1.get(0).get("count(*)");
			
			List<Map<String, Object>> rs2 = cmd.executeQuery("SELECT count(*) FROM thread, post WHERE thread.forum_id = " + forumId + " AND thread.id = post.thread_id");
			String postNum = (String)rs2.get(0).get("count(*)");
			
			String forumStats = forum_name + "\t\t\t:" + threadNum + "\t" + postNum;
			result.add(forumStats);
			System.out.println(forumStats);
			
		}
		
		cmd.closeConnection();
		return result;
	}

	public static void main(String[] args){
		List<String> forumStats = DBStatistics.getForumStatistics("honda_tech", "localhost", 3306, "xzhang", "hitachi");
		for(String stat : forumStats){
			System.err.println(stat);
		}
	}
}
