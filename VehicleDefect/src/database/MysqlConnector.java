package database;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * set mysql parameters in this class
 * @author Xuan Zhang
 * 			 
 */
public class MysqlConnector {
	// database name
	public static final String DEFAULT_DBNAME = "";
	// database host ip
	public static final String DEFAULT_DBHOST = "";
	// database port
	public static final int DEFAULT_DBPORT = 3306;
	// database user name
	public static final String DEFAULT_DBUSER = "";
	// database user password
	public static final String DEFAULT_DBPASSWORD = "";
	
	public static final String MYSQL_Driver = "com.mysql.jdbc.Driver";
	public static final String MYSQL_CONPrefix = "jdbc:mysql://";
	
	private String dbName;
	private String dbHost;
	private int dbPort;
	private String dbUser;
	private String dbPassword;
	
	/**
	 * get connection from mysql
	 * @return Connection
	 */
	public Connection getConnection() {
		return getConnection(null, null, 0, null, null, MYSQL_Driver, MYSQL_CONPrefix);
	}
	
	/**
	 * Get connection with the specified properties
	 * 
	 * @param curDbName
	 * @param curDbHost
	 * @param curDbPort
	 * @param curDbUser
	 * @param curDbPassword
	 * @return
	 */
	public Connection getConnection(String curDbName, String curDbHost, 
			int curDbPort, String curDbUser, String curDbPassword){
		return getConnection(curDbName, curDbHost, 
				curDbPort, curDbUser, curDbPassword, 
				MYSQL_Driver, MYSQL_CONPrefix);
	}
	
	/**
	 * get Connection given driver and connection prefix of certain database
	 * 
	 * @return Connection
	 */
	public Connection getConnection(String curDbName, String curDbHost, 
			int curDbPort, String curDbUser, String curDbPassword, 
			String driver, String conPrefix) {
		this.dbName = curDbName;
		this.dbHost = curDbHost;
		this.dbPort = curDbPort;
		this.dbUser = curDbUser;
		this.dbPassword = curDbPassword;
		
		if (curDbName == null)
			this.dbName = DEFAULT_DBNAME;
		if(curDbHost == null)
			this.dbHost = DEFAULT_DBHOST;
		if(curDbPort <= 0)
			this.dbPort = DEFAULT_DBPORT;
		if(curDbUser == null)
			this.dbUser = DEFAULT_DBUSER;
		if(curDbPassword == null)
			this.dbPassword = DEFAULT_DBPASSWORD;
		
		try {
			Class.forName(driver);
			Connection con = null;

			String conString = conPrefix + this.dbHost + ":" + this.dbPort + "/" + this.dbName +"?autoReconnect=true";

			con = DriverManager.getConnection(conString, this.dbUser, this.dbPassword);
			if (con != null) {
				con.setAutoCommit(true);
				return con;
			}
			return con;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
