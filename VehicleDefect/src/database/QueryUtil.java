package database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.PostRecord;

public class QueryUtil {
	
	
	/**
	 * Extract post records (the 1st post of each thread) from DB according to the specified forumID
	 * 
	 * @param forumID
	 * @return
	 */
	public static List<PostRecord> extract1stPostsFromDB(SQLCMD db, int forumID) {
		String sql = "select thread.thread_title, post.id, post.time, post.content "
				+ "from post, thread where post.thread_id = thread.id and thread.forum_id = "
				+ forumID + " group by post.thread_id LIMIT 0, 10000000";
		List<Map<String, Object>> rows = db.executeQuery(sql);
		List<PostRecord> result = new ArrayList<PostRecord>();
		for(Map<String, Object> map : rows){
			String title = (String)map.get("thread_title");
			String time = (String)map.get("time");
			String content = (String)map.get("content");
			String id = (String)map.get("id");
			System.out.println(id + "\t" + title + "\t" + time);
			PostRecord ps = new PostRecord(title, time, content, id);
			result.add(ps);
		}
		
		System.out.println("\n" + result.size() + " records extracted from DB\n");
		return result;
	}
	
	public static List<PostRecord> extractPostsFromDB(SQLCMD db, int forumID) {
		String sql = "select thread.thread_title, post.id, post.time, post.content "
				+ "from post, thread where post.thread_id = thread.id and thread.forum_id = "
				+ forumID + " LIMIT 0, 10000000";
		List<Map<String, Object>> rows = db.executeQuery(sql);
		List<PostRecord> result = new ArrayList<PostRecord>();
		for(Map<String, Object> map : rows){
			String title = (String)map.get("thread_title");
			String time = (String)map.get("time");
			String content = (String)map.get("content");
			String id = (String)map.get("id");
			System.out.println(id + "\t" + title + "\t" + time);
			PostRecord ps = new PostRecord(title, time, content, id);
			result.add(ps);
		}
		
		System.out.println("\n" + result.size() + " records extracted from DB\n");
		return result;
	}

}
