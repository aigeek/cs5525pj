package Shuoniu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import preprocess.StanfordNLP;

import model.PostRecord;

public class InstanceList {
	Map<String, StatInstance> list = new HashMap<String, StatInstance>();// hashmap
																			// of
																			// all
																			// car
																			// info

	public Map<String, StatInstance> getList() {
		return list;
	}

	public void AddInstance(StatInstance ins) {
		String key = ins.getKey();
		if (list.containsKey(key)) {
			Instance instance = list.get(key);
			for (PostRecord pr : ins.getList()) {
				// instance.addPost(pr, stan);
				instance.addPost(pr);
			}
		} else {
			list.put(key, ins);
		}
	}

	public void AddInstance(StatInstance ins, StanfordNLP stan) {
		String key = ins.getKey();
		if (list.containsKey(key)) {
			Instance instance = list.get(key);
			for (PostRecord pr : ins.getList()) {
				instance.addPost(pr, stan);
			}
		} else {
			list.put(key, ins);
		}
	}

	public List<StatInstance> getInstanceByCarInfo(String make, Model model,
			int year) {
		List<StatInstance> siList = new ArrayList<StatInstance>();
		for (int i = year; i <= 2014; i++) {
			String key = make + "_" + model.name() + "_" + year + "_" + i;
			if (list.containsKey(key)) {
				siList.add(list.get(key));
			}
		}
		return siList;
	}
}
