package Shuoniu;

import java.util.HashMap;
import java.util.Map;

public class CBInstanceList {
	Map<String, CBInstance> list = new HashMap<String, CBInstance>();

	public Map<String, CBInstance> getList() {
		return list;
	}

	public void AddInstance(CBInstance ins) throws Exception {
		String key = ins.getKey();
		if (list.containsKey(key)) {
			throw new Exception("Record duplicated");
		} else {
			list.put(key, ins);
		}
	}
}
