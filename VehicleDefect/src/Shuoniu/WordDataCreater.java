package Shuoniu;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.BigramRecord;
import model.Document;
import model.PostRecord;
import preprocess.StanfordNLP;
import utility.CSVParser;
import utility.TextUtil;
import console.GlobalConstants;
import database.SQLCMD;
import feature.ChiSquareSelector;

public class WordDataCreater {
	private Map<String, Integer> featureList = new HashMap<String, Integer>();
	private Map<String, Integer> featureListAll = new HashMap<String, Integer>();
	private InstanceList instanceList = new InstanceList();
	private CBInstanceList cbInstanceList = new CBInstanceList();
	private Map<String, Integer> cbRecord = new HashMap<String, Integer>();
	private int STARTYEAR = 2003;
	private int ENDYEAR = 2014;
	private String make = "Toyota";
	private SQLCMD db;
	private int MINFREQ = 5;
	private List<Document> documentList = new ArrayList<Document>();
	private List<String> selectedFeatures;
	private List<String> smokeFeatures = new ArrayList<String>();
	private int FEATURETOSELECT = 100;
	StanfordNLP stan = StanfordNLP.getInstance();

	public void init(SQLCMD db) {
		this.db = db;
	}

	public void genSmokeWordList() {
		String[] smokeWordColumns = { "bigram", "rsv" };
		List<Object> bigram = CSVParser.retrieveDataFromCSV(
				GlobalConstants.SMOKE_BIGRAM_URL, BigramRecord.class,
				smokeWordColumns);
		for (Object obj : bigram) {
			BigramRecord br = (BigramRecord) obj;
			String bgText = br.getBigram();
			smokeFeatures.add(bgText);
		}
	}

	public void genInstance() {
		String sql = "select forum_name,time,content "
				+ "from toyota.post, toyota.forums,toyota.thread "
				+ "where toyota.post.thread_id=toyota.thread.id and toyota.thread.forum_id=toyota.forums.id "
				// +
				// "and toyota.thread.forum_id<27 and toyota.thread.forum_id>20 "
				+ "limit 2000000;";
		List<Map<String, Object>> rows = db.executeQuery(sql);
		Calendar calendar = Calendar.getInstance();
		int counter = 0;
		for (Map<String, Object> map : rows) {
			if (counter % 100 == 0)
				System.out.println("Processing record " + counter + "/"
						+ rows.size());
			counter++;
			String content = (String) map.get("content");
			String time = (String) map.get("time");
			String forum = (String) map.get("forum_name");
			PostRecord pr = new PostRecord();
			pr.setTime(time);
			pr.setContent(content);
			StatInstance inst = new StatInstance();
			inst.addPost(pr);
			// Abstract post year;
			int postYear = 0;
			if (time.indexOf("day") >= 0) {
				postYear = 2014;
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM-DD-YYYY, HH:mm a");
				try {
					Date date = sdf.parse(time.trim());
					calendar.setTime(date);
					postYear = calendar.get(Calendar.YEAR);
					inst.setYear(postYear);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			inst.postYear = postYear;
			// Abstract make model and year
			Model model = SNUtility.ExtractModel(forum);
			int year = TextUtil.extractYear(content);

			if (model != null && year >= STARTYEAR && year <= ENDYEAR
					&& year <= inst.postYear) {
				inst.make = make;
				inst.model = model;
				inst.year = year;
				instanceList.AddInstance(inst, stan);
			}
		}
	}

	public void genInstancePart2() {
		// Create word List
		for (StatInstance i : instanceList.getList().values()) {
			List<String> words = stan.lemmatizeAndFilterStopWord(i.getText(),
					true, true);
			for (String w : words) {
				featureList.put(w, 0);
				featureListAll.put(w, 0);
			}
			System.out.println("Post: " + i.getKey() + " # "
					+ i.getList().size());
			i.setWordList(words);
		}

		// Remove the less frequent words
		for (StatInstance i : instanceList.getList().values()) {
			List<String> words = i.getWordList();
			for (String w : words) {
				if (featureListAll.containsKey(w)) {
					featureListAll.put(w, featureListAll.get(w) + 1);
				}
			}
		}
		for (String key : featureListAll.keySet()) {
			if (featureListAll.get(key) < MINFREQ) {
				featureList.remove(key);
			} else {
				System.out.println("Key " + key + " selected");
			}
		}
		// Compute the word frequency
		for (StatInstance i : instanceList.getList().values()) {

			List<String> words = i.getWordList();
			ZeroWordList();
			for (String w : words) {
				if (featureList.containsKey(w)) {
					int updValue = featureList.get(w);
					featureList.put(w, updValue + 1);
					// featureList.put(w, 1);
				}
			}
			Double[] wordFreq = new Double[featureList.size()];
			int index = 0;
			for (String key : featureList.keySet()) {
				wordFreq[index++] = (double) featureList.get(key).intValue();
			}
			i.setWordFrequency(wordFreq);
			System.out.println("Init instance: " + i.getKey());
		}
	}

	public void genInstancePart2_1() {
		// Create word List
		for (StatInstance i : instanceList.getList().values()) {
			List<String> words = new ArrayList<String>();
			for (List<String> swl : i.getPrWordList()) {
				for (String str : swl) {
					if (!words.contains(str)) {
						words.add(str);
					}
				}
			}
			System.out.println("Post: " + i.getKey() + " # "
					+ i.getList().size());
			for (String w : words) {
				for (String sw : smokeFeatures) {
					if (sw.toLowerCase().indexOf(w) >= 0) {
						featureList.put(w, 0);
					}
				}
			}
			i.setWordList(words);
		}

		// Compute the word frequency
		for (StatInstance i : instanceList.getList().values()) {
			ZeroWordList();
			for (List<String> pr : i.getPrWordList()) {
				Map<String, Integer> temp = new HashMap<String, Integer>();
				for (String w : pr) {
					if (featureList.containsKey(w)) {
						temp.put(w, 1);
						// if (temp.containsKey(w)) {
						// temp.put(w, temp.get(w) + 1);
						// } else {
						// temp.put(w, 1);
						// }
					}
				}
				for (String s : temp.keySet()) {
					featureList.put(s, featureList.get(s) + temp.get(s));
				}
			}

			Double[] wordFreq = new Double[featureList.size()];
			int index = 0;
			for (String key : featureList.keySet()) {
				wordFreq[index++] = new Double(featureList.get(key).intValue());
			}
			i.setWordFrequency(wordFreq);
			System.out.println("Init instance: " + i.getKey());
		}

	}

	public void loadInstanceFromFile(String file) {
		String templist = "";
		try {
			templist = new String(readAllBytes((get(file))));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		featureList.clear();
		instanceList = new InstanceList();
		if (templist.length() > 0) {
			String[] insts = templist.split("\r");
			for (int i = 0; i < insts.length; i++) {
				String[] info = insts[i].split(",");
				if (i == 0) {
					for (int j = 1; j < info.length; j++) {
						featureList.put(info[j], 0);
					}
				} else {
					StatInstance sinst = new StatInstance();
					String[] carInfo = info[0].split("_");
					sinst.setMake(carInfo[0]);
					sinst.setModel(SNUtility.ExtractModel(carInfo[1]));
					sinst.setYear(Integer.parseInt(carInfo[2]));
					sinst.setPostYear(Integer.parseInt(carInfo[3]));
					Double[] wrFreq = new Double[info.length - 1];
					for (int j = 1; j < info.length; j++) {
						wrFreq[j - 1] = new Double(info[j]);
					}
					sinst.setWordFrequency(wrFreq);
					instanceList.AddInstance(sinst);
					System.out.println("Loaded instance: " + sinst.getKey());
				}
			}
		}
	}

	private void InitCBRecord() {
		String sql = "SELECT MAKETXT,MODELTXT,YEARTXT,DATEA FROM toyota.flat_rcl where MAKETXT like '%TOYOTA%' LIMIT 200000;";
		List<Map<String, Object>> rows = db.executeQuery(sql);
		// DATEA value is unique
		for (Map<String, Object> map : rows) {
			String datea = ((String) map.get("DATEA")).substring(0, 4);
			String model = (String) map.get("MODELTXT");
			String year = (String) map.get("YEARTXT");
			Model mo = SNUtility.ExtractModel(model);
			if (mo != null) {
				String key = make + "_" + mo.name() + "_" + year + "_" + datea;
				if (cbRecord.containsKey(key)) {
					cbRecord.put(key, cbRecord.get(key) + 1);
				} else {
					cbRecord.put(key, new Integer(1));
				}
				System.out.println("Init Record: " + key + "  "
						+ cbRecord.get(key));
			}
		}
	}

	public void genCBInstance() {
		// Generate Instance
		InitCBRecord();
		for (int y = STARTYEAR; y <= ENDYEAR; y++) {
			for (Model m : Model.values()) {
				List<StatInstance> siList = instanceList.getInstanceByCarInfo(
						make, m, y);
				for (int z = y + 2; z <= ENDYEAR; z++) {
					CBInstance cbinst = new CBInstance();
					cbinst.setMake(make);
					cbinst.setModel(m);
					cbinst.setYear(y);
					boolean isValid = cbinst.setCallBackYear(z);
					if (isValid) {
						try {
							int hasRecord = cbinst.associateData(siList);
							if (hasRecord > 0) {
								int cbNum = GetRecordNum(make, m, y,
										cbinst.getCallBackYear());
								if (cbNum > 0) {
									cbinst.setIsCallback("Y");
								} else {
									cbinst.setIsCallback("N");
								}
								cbInstanceList.AddInstance(cbinst);
								System.out.println("Init CB instance: "
										+ cbinst.getKey() + " Y/N: "
										+ cbinst.getIsCallback());
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private int GetRecordNum(String make, Model model, int year, int cbyear) {
		String key = make + "_" + model.name() + "_" + year + "_" + cbyear;
		int result = 0;
		if (cbRecord.containsKey(key)) {
			result = cbRecord.get(key);
		} else {
			result = 0;
		}
		return result;
	}

	public void loadCBInstanceFromFile(String file) {
		String templist = "";
		try {
			templist = new String(readAllBytes((get(file))));
		} catch (IOException e) { // TODO Auto-generated catch
			e.printStackTrace();
		}
		featureList.clear();
		cbInstanceList = new CBInstanceList();
		if (templist.length() > 0) {
			String[] insts = templist.split("\r");
			for (int i = 0; i < insts.length; i++) {
				String[] info = insts[i].split(",");
				if (i == 0) {
					for (int j = 1; j < info.length - 1; j++) {
						featureList.put(info[j], 0);
					}
				} else {
					CBInstance sinst = new CBInstance();
					String[] carInfo = info[0].split("_");
					sinst.setMake(carInfo[0]);
					sinst.setModel(SNUtility.ExtractModel(carInfo[1]));
					sinst.setYear(Integer.parseInt(carInfo[2]));
					sinst.setCallBackYear(Integer.parseInt(carInfo[3]));
					Double[] wrFreq = new Double[info.length - 2];
					sinst.setIsCallback(info[info.length - 1].trim());
					for (int j = 1; j < info.length - 1; j++) {
						wrFreq[j - 1] = new Double(info[j]);
					}
					sinst.setWordFrequency(wrFreq);
					try {
						cbInstanceList.AddInstance(sinst);
						System.out
								.println("Add CB Record:" + sinst.getKey()
										+ " length: "
										+ sinst.getWordFrequency().length);
					} catch (Exception e) { // TODO Auto-generated catch
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void genDocumentList() {
		for (CBInstance cbinst : cbInstanceList.getList().values()) {
			Document doc = new Document("", "", "", "");
			int[] frequencies = new int[cbinst.getWordFrequency().length];
			System.out.println("Create Document:" + cbinst.getKey());
			for (int i = 0; i < frequencies.length; i++) {
				frequencies[i] = (int) (cbinst.getWordFrequency()[i]
						.doubleValue());
			}
			doc.setTermFrequencies(frequencies);
			doc.setCategoryStr("" + cbinst.getIsCallback());
			documentList.add(doc);
		}
	}

	public void selectFeature() {
		ChiSquareSelector chisquare = new ChiSquareSelector();
		selectedFeatures = chisquare.selectFeaturesForClass(documentList,
				featureList.keySet(), FEATURETOSELECT, "Y");

		boolean[] selectState = new boolean[featureList.size()];
		int index = 0;
		for (String key : featureList.keySet()) {
			selectState[index++] = selectedFeatures.contains(key);
		}
		for (CBInstance cbinst : cbInstanceList.getList().values()) {
			cbinst.setSelectList(selectState);
		}
	}

	public void saveFullFrequencyFile(String file, String instToSave) {
		try {
			File f = new File(file);
			if (f.exists()) {
				f.delete();
			}
			f.createNewFile();
			FileWriter writer = new FileWriter(f);
			writer.append("SHUONIU");
			for (String s : featureList.keySet()) {
				if (instToSave.equals("select")) {
					if (selectedFeatures.contains(s)) {
						writer.append("," + s);
					}
				} else {
					writer.append("," + s);
				}
			}
			if (instToSave.equals("callback") || instToSave.equals("select")) {
				writer.append(",MY_CLASS");
			}
			writer.append("\r");
			if (instToSave.equals("post")) {
				for (StatInstance i : instanceList.getList().values()) {
					writer.append(i.getKey());
					for (Double freq : i.getWordFrequency()) {
						writer.append("," + freq);
					}
					writer.append("\r");
				}
			} else if (instToSave.equals("callback")) {
				for (CBInstance i : cbInstanceList.getList().values()) {
					System.out.println("Create Record: " + i.getKey()
							+ " Value: " + i.getIsCallback());
					writer.append(i.getKey());
					for (Double freq : i.getWordFrequency()) {
						writer.append("," + freq);
					}
					writer.append("," + i.getIsCallback());
					writer.append("\r");
				}
			} else if (instToSave.equals("select")) {
				for (CBInstance i : cbInstanceList.getList().values()) {
					System.out.println("Create Record: " + i.getKey()
							+ " Value: " + i.getIsCallback());
					writer.append(i.getKey());
					Double[] freq = i.getWordFrequency();
					boolean[] select = i.getSelectList();
					for (int m = 0; m < freq.length; m++) {
						if (select[m]) {
							writer.append("," + freq[m]);
						}
					}
					writer.append("," + i.getIsCallback());
					writer.append("\r");
				}
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void ZeroWordList() {
		for (String s : featureList.keySet()) {
			featureList.put(s, 0);
		}
	}
}
