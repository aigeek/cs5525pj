package Shuoniu;

import database.SQLCMD;

public class Main {

	/**
	 * @param args
	 */
	private static SQLCMD db = new SQLCMD("toyota", "128.173.49.48", 3306,
			"dapj", "cs5525");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WordDataCreater wordgen = new WordDataCreater();
		wordgen.init(db);

		 wordgen.genSmokeWordList();
		 wordgen.genInstance();
		 wordgen.genInstancePart2_1();
		 wordgen.saveFullFrequencyFile(
		 "result/post.csv", "post");

		wordgen.loadInstanceFromFile("result/post.csv");
		wordgen.genCBInstance();
		wordgen.saveFullFrequencyFile(
				"result/callback.csv", "callback");

		wordgen.loadCBInstanceFromFile("result/callback.csv");
		wordgen.genDocumentList();
		wordgen.selectFeature();
		wordgen.saveFullFrequencyFile(
				"result/select.csv", "select");
	}
}
