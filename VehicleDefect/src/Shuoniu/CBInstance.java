package Shuoniu;

import java.util.ArrayList;
import java.util.List;

public class CBInstance extends StatInstance {
	protected int callBackYear;
	protected int YEARTOCOUNT = 3;
	private String callback = "";// 0 Nay 1 Yay

	public String getIsCallback() {
		return callback;
	}

	public void setIsCallback(String isCallback) {
		this.callback = isCallback;
	}

	List<StatInstance> relatedInstances = new ArrayList<StatInstance>();

	public int getCallBackYear() {
		return callBackYear;
	}

	// Return false if no sufficient data available.
	public boolean setCallBackYear(int callBackYear) {
		if (callBackYear - this.year < YEARTOCOUNT) {
			this.callBackYear = 0;
			return false;
		} else {
			this.callBackYear = callBackYear;
			return true;
		}
	}

	public List<StatInstance> getRelatedInstances() {
		return relatedInstances;
	}

	public String getKey() {
		return make + "_" + model.name() + "_" + year + "_" + this.callBackYear;
	}

	public int associateData(List<StatInstance> list) throws Exception {
		int postNum = 0;
		for (StatInstance si : list) {
			if (si.getMake().equals(this.make)
					&& si.getModel().equals(this.model)
					&& si.getYear() == this.year) {
				if (callBackYear > si.postYear
						&& callBackYear - si.postYear <= YEARTOCOUNT) {
					relatedInstances.add(si);
					postNum++;
				}
			}
		}
		if (relatedInstances.size() > 0) {
			Double[] wFreq = new Double[relatedInstances.get(0)
					.getWordFrequency().length];
			for (int i = 0; i < wFreq.length; i++) {
				wFreq[i] = 0.0;
			}
			for (StatInstance statInst : relatedInstances) {
				Double[] nextRecord = statInst.getWordFrequency();
				if (nextRecord != null && wFreq.length == nextRecord.length) {
					for (int j = 0; j < nextRecord.length; j++) {
						wFreq[j] += nextRecord[j];
					}
				} else {
					throw new Exception("Unmatched candidate world lists");
				}
			}
			this.wordFrequency = wFreq;
		}
		return postNum;
	}
}
