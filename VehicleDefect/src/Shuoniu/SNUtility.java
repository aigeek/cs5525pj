package Shuoniu;

public class SNUtility {
	public static Model ExtractModel(String forum) {
		Model[] model = Model.values();
		for (Model m : model) {
			String name = m.name().toLowerCase();
			if (forum.toLowerCase().indexOf(name) >= 0) {
				return m;
			}
		}
		return null;
	}
}
