package Shuoniu;

import java.util.ArrayList;
import java.util.List;

public class StatInstance extends Instance {
	protected List<String> wordList = new ArrayList<String>();// Candidate
																// Features
	protected Double[] wordFrequency;// Number of Appearance
	protected boolean[] selectList;// Corresponding word

	public boolean[] getSelectList() {
		return selectList;
	}

	public void setSelectList(boolean[] selectList) {
		this.selectList = selectList;
	}

	public Double[] getWordFrequency() {
		return wordFrequency;
	}

	public void setWordFrequency(Double[] wordFrequency) {
		this.wordFrequency = wordFrequency;
	}

	public List<String> getWordList() {
		return wordList;
	}

	public void setWordList(List<String> wordList) {
		this.wordList = wordList;
	}
}
