package Shuoniu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import preprocess.StanfordNLP;

import model.PostRecord;

public class Instance {

	/**
	 * @param args
	 */
	protected String make = "";
	protected Model model;
	protected int year = 0;
	protected int postYear = 0;
	protected List<PostRecord> list = new ArrayList<PostRecord>();
	protected List<List<String>> prWordList = new ArrayList<List<String>>();
	

	public List<List<String>> getPrWordList() {
		return prWordList;
	}

	public void setPrWordList(List<List<String>> prWordList) {
		this.prWordList = prWordList;
	}

	protected String text = "";

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getPostYear() {
		return postYear;
	}

	public void setPostYear(int postYear) {
		this.postYear = postYear;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public List<PostRecord> getList() {
		return list;
	}

	public void addPost(PostRecord record) {
		list.add(record);
	}

	public void addPost(PostRecord record, StanfordNLP stan) {
		list.add(record);
		prWordList.add(stan.lemmatizeAndFilterStopWord(record.getContent(),
				true, true));
	}

	public String getText() {
		if (text.length() == 0) {
			for (PostRecord p : list) {
				text += p.getContent();
			}
		}
		return text;
	}

	public String getKey() {
		return make + "_" + model.name() + "_" + year + "_" + postYear;
	}
}
